import { LitElement, css, html } from "lit";

export class ClassName extends LitElement {
  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html``;
  }

  static get styles() {
    return css``;
  }
}

customElements.define("my-component", ClassName);
