import { LitElement, css, html } from "lit";

export class LoginComponent extends LitElement {
  constructor() {
    super();
    this.active = false;
  }

  render() {
    return html`
      <div class="wrapper ${this.active ? "active" : ""}">
        <div class="form-box login">
          <h2>Inicio de Sesion</h2>
          <form action="#">
            <div class="input-box">
              <span class="icon">
                <ion-icon name="mail"></ion-icon>
              </span>
              <input type="" required />
              <label for="">Correo</label>
            </div>
            <div class="input-box">
              <span class="icon">
                <ion-icon name="lock-closed"></ion-icon>
              </span>
              <input type="password" required />
              <label for="">Contraseña</label>
            </div>
            <button type="button" class="btn">Iniciar Sesiòn</button>
            <div class="login-register">
              <p class="login-register">
                ¿No tienes una cuenta?
                <a href="#" class="login-register" @click=${
                  this.changeState
                }>Crear cuenta nueva</a>
              </p>
            </div>
          </form>
        </div>

        <div class="form-box register">
          <h2>Inicio de Sesion</h2>
          <form action="#">
 <div class="input-box">
              <span class="icon">
               <ion-icon name="person"></ion-icon>
              </span>
              <input type="text" required />
              <label for="">Nombre</label>
            </div>

            <div class="input-box">
              <span class="icon">
                <ion-icon name="mail"></ion-icon>
              </span>
              <input type="" required />
              <label for="">Correo</label>
            </div>
            <div class="input-box">
              <span class="icon">
                <ion-icon name="lock-closed"></ion-icon>
              </span>
              <input type="password" required />
              <label for="">Contraseña</label>
            </div>
            <button type="button" class="btn">Registrar</button>
            <div class="login-register">
              <p class="login-register">
                ¿Ya tienes una cuenta?
                <a href="#" class="login-register" @click=${
                  this.changeState
                }>Iniciar sesion </a>
              </p>
            </div>


      </div>
    `;
  }

  changeState() {
    this.active = !this.active;
    this.requestUpdate();
  }

  static get styles() {
    return css`
      .wrapper {
        position: relative;
        width: 400px;
        height: 440px;
        background: transparent;
        border: 2px solid rgba(255 255 255 0.5);
        border-radius: 20px;
        backdrop-filter: blur(20px);
        box-shadow: 0 0 30px rgba(0, 0, 0, 0.5);
        display: flex;
        justify-content: center;
        align-items: center;
        transition: height 0.2s ease;
        overflow:hidden;
      }
      .wrapper.active {
        height: 520px;
      }
      .wrapper .form-box {
        width: 100%;
        padding: 40px;
      }
      .wrapper .form-box.login {
        transition: transform 0.18s ease;
        transform: translateX(0);
      }
      .wrapper.active .form-box.login {
        transition: none;
        transform: translateX(-400px);
        position:absolute;
      }

      .wrapper .form-box.register {
        position: absolute;
        transition: none;
        transform: translateX(400px);
      }

      .wrapper.active .form-box.register {
        position:relative;
        transition: transform 0.18s ease;
        transform: translateX(0);
      }

      .form-box h1 {
        font-size: 2em;
        color: black;
        text-align: center;
      }
      .input-box {
        position: relative;
        width: 100%;
        height: 50px;
        border-bottom: 2px solid #162938;
        margin: 30px 0;
      }
      .input-box label {
        position: absolute;
        top: 50%;
        left: 5px;
        transform: translateY(-50%);
        font-size: 1em;
        color: #162938;
        font-weight: 500;
        pointer-eventes: none;
        transition: 0.5s;
      }
      .input-box input:focus ~ label,
      .input-box input:valid ~ label {
        top: -5px;
      }
      .input-box input {
        width: 100%;
        height: 100%;
        background: transparent;
        border: none;
        outline: none;
        font-size: 1em;
        color: #162938;
        font-weight: 600;
        padding: 0 35px 0 5px;
      }
      .input-box .icon {
        position: absolute;
        right: 8px;
        font-size: 1.2em;
        color: #162938;
        line-height: 57px;
      }
      .remember-forgot {
        font-size: 0.9em;
        color: #162938;
        font-weight: 500;
        margin: -15px 0 15px;
        display: felx;
        justify-content: space-between;
      }
      .remember-forgot a {
        color: #162938;
        text-decoration: none;
      }
      .remember-forgot a:hover {
        text-decoration: underline;
      }
      .btn {
        width: 100%;
        height: 45px;
        background: #162938;
        border: none;
        outline: none;
        border-radius: 6px;
        cursor: pointer;
        font-size: 1em;
        color: #fff;
      }
      .login-register {
        font-size: 14px;
        color: #162938;
        text-align: center;
        text-decoration: none;
        font-weight: 500;
        margin: 25px 0 10px;
      }

      .login-register p a {
        color: #162938;
        text-decoration: none;
        font-weight: 600;
      }

      .login-register p a:hover {
        text-decoration: underline;
      }
    `;
  }
}

customElements.define("login-component", LoginComponent);
