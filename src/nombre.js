import { LitElement, css, html } from "lit";

export class MyName extends LitElement {
  static get properties() {
    return {};
  }

  constructor() {
    super();
    this.name = "";
    this.lastName = "";
  }

  // Esta es la primera funcion que se ejecuta cuando el componente basado en lit es generado
  firstUpdated() {
    this.inputName = this.renderRoot?.querySelector(".name");
    this.inputLastName = this.renderRoot?.querySelector(".lastname");
  }

  render() {
    return html`
      <h1>Bienvenido</h1>
      <form action="">
        <label>
          Nombre <input type="text" class="name" @input=${this.changeName}
        /></label>
        <label
          >Apellido<input
            type="text"
            class="lastname"
            @keyup=${this.changeName}
        /></label>
      </form>
      <p>su nombre es ${this.name} ${this.lastName}</p>
    `;
  }
  //funcion de eventos
  changeName() {
    console.log(this.inputLastName);
    this.name = this.inputName.value;
    this.lastName = this.inputLastName.value;
    this.requestUpdate();
  }

  static get styles() {
    return css``;
  }
}

customElements.define("my-name", MyName);
